package org.projectunsc.cicero.extensions

/**
 * CharArrays
 * @author Gregory Maddra
 * 2017-05-30
 */

/**
 * Gets a substring from index i to j
 * @param i Starting point
 * @param j End point (inclusive)
 */
operator fun String.get(i: Int, j: Int): CharArray {
    if (i >= j)
        throw IllegalArgumentException("Inputs must form a valid ascending range")
    var array = CharArray(j - i)
    for (k in i..j) {
        array += this[k]
    }
    return array
}


