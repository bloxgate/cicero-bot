package org.projectunsc.cicero.web

import com.beust.jcommander.IParameterValidator
import com.beust.jcommander.Parameter
import com.beust.jcommander.ParameterException
import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpHandler
import com.sun.net.httpserver.HttpServer
import discord4j.core.`object`.entity.TextChannel
import discord4j.core.`object`.util.Snowflake
import org.projectunsc.cicero.Constants
import org.projectunsc.cicero.Main
import java.net.InetSocketAddress
import java.net.URLDecoder

object WebInterface {

    @Parameter(names = ["--webInterfacePort", "--port", "-p"], required = true, validateWith = [PortValidator::class])
    var interfacePort: Int = 0
        set(value) {
            //Only let this be set once
            if (field == 0)
                field = value
        }

    class PortValidator : IParameterValidator {
        override fun validate(name: String, value: String) {
            val port = value.toInt()
            if (port <= 0 || port > 65535) {
                throw ParameterException("Parameter $name must be a valid TCP port")
            }
        }
    }

    val webInterface = HttpServer.create()

    fun start() {
        //Bind to port
        webInterface.bind(InetSocketAddress(interfacePort), 0)

        //Register pages
        webInterface.createContext("/newnote", NoteReportHandler)
        webInterface.createContext("/ahelp", AdminHelpHandler)
        webInterface.createContext("/relayMessage", MessageRelayHandler)

        //Start Server
        webInterface.executor = null
        webInterface.start()
    }

    fun stop() {
        webInterface.stop(0)
    }

    fun handleGetParameters(parameters: String?): Map<String, String> {
        if (parameters == null) return mapOf()
        return parameters.split('&').map { it.split('=') }.associate { it[0] to it[1] }
                .mapValues { URLDecoder.decode(it.value, Charsets.ISO_8859_1.name()) }
    }

    private object NoteReportHandler : HttpHandler {

        override fun handle(exchange: HttpExchange) {
            if (!exchange.requestMethod.equals("get", true)) {
                exchange.sendResponseHeaders(405, 0)
                exchange.close()
                return
            }

            val parameters = handleGetParameters(exchange.requestURI.query)
            if (parameters.getOrDefault("key", "") != Constants.COMMS_KEY) {
                exchange.sendResponseHeaders(403, 0)
                exchange.close()
                return
            }

            if (parameters["note"] != null) {
                exchange.sendResponseHeaders(204, -1)
                exchange.close()

                val channel = Main.client.getChannelById(Snowflake.of(582687884226592769)).block() as TextChannel //Cicero alerts channel
                channel.createMessage { it.setContent(URLDecoder.decode(parameters["note"], Charsets.ISO_8859_1.name())) }.subscribe()

                return
            }

            exchange.sendResponseHeaders(400, 0)
            exchange.close()
        }

    }

}