package org.projectunsc.cicero.commands.public

import discord4j.core.`object`.entity.Guild
import discord4j.core.`object`.entity.TextChannel
import discord4j.core.`object`.entity.User
import org.projectunsc.cicero.commands.AbstractCommand

/**
 * RollTroopsCommand
 * @author Gregory Maddra
 * 2017-10-25
 */
object RollTroopsCommand : AbstractCommand() {

    override val command = "rolltroops"
    override val permissionType = CommandPermissionType.CHANNEL

    init {
        allowedChannels.add("dice-roll")
        allowedChannels.add("rp-channel")
    }

    override fun execute(command: List<String>, user: User, channel: TextChannel, guild: Guild): Boolean {
        if (!canExecute(user, channel, guild)) {
            val builder = StringBuilder()
            builder.appendln("${user.mention}, this command can only be run in the following channels: ")
            allowedChannels.forEach {
                builder.appendln("#$it")
            }
            channel.createMessage { it.setContent(builder.toString()) }.subscribe()
            return true
        }

        val author = user.mention

        try {
            val times = command.getOrNull(2)?.toInt()

            var zeros = 0
            var ones = 0
            var total = 0
            if (times != 0 && times != null) {
                for (i in 1..times) {
                    val random = Math.round(Math.random()).toInt()
                    total += random
                    if (random == 0) {
                        zeros += 1
                    } else {
                        ones += 1
                    }
                }
            }
            channel.createMessage { it.setContent("$author $total damage. $zeros did no damage. $ones did damage.") }.subscribe()
        } catch (ex: NumberFormatException) {
            channel.createMessage { it.setContent(user.mention + ", you must enter a valid number of times to roll!") }.subscribe()
        }

        return true
    }
}