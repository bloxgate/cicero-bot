package org.projectunsc.cicero.commands.public

import discord4j.core.`object`.entity.Guild
import discord4j.core.`object`.entity.TextChannel
import discord4j.core.`object`.entity.User
import discord4j.core.`object`.util.Snowflake
import org.projectunsc.cicero.Constants
import org.projectunsc.cicero.commands.AbstractCommand

object UnsubscribeCommand : AbstractCommand() {
    override val permissionType: CommandPermissionType = CommandPermissionType.DEFAULT
    override val command: String = "unsubscribe"

    override fun execute(command: List<String>, user: User, channel: TextChannel, guild: Guild): Boolean {
        if (!canExecute(user, channel, guild)) {
            return false
        }

        user.asMember(guild.id).subscribe { member ->
            member.removeRole(Snowflake.of(Constants.ROUND_SUBSCRIBER_ROLE_ID)).subscribe {
                throw Exception("Error removing round subscriber role!")
            }
            channel.createMessage("${user.mention}, you have been unsubscribed from round startup alerts").subscribe()
        }

        return true
    }
}