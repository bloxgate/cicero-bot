package org.projectunsc.cicero.commands.staff

import discord4j.core.`object`.entity.Guild
import discord4j.core.`object`.entity.TextChannel
import discord4j.core.`object`.entity.User
import me.gmaddra.byond4k.ByondPacket
import me.gmaddra.byond4k.ByondResponsePacket
import org.projectunsc.cicero.Constants
import org.projectunsc.cicero.commands.AbstractCommand
import java.io.DataInputStream
import java.net.ConnectException
import java.net.Socket
import java.net.URLEncoder

/**
 * NotesCommand
 * Allows staff to retrieve the admin notes on a player
 * @author Gregory Maddra
 * 2017-12-23
 */
object NotesCommand : AbstractCommand() {

    override val permissionType: CommandPermissionType = CommandPermissionType.CHANNEL
    override val command = "notes"

    init {
        allowedChannels.add("cicero-logs-notes-alerts")
    }

    override fun execute(command: List<String>, user: User, channel: TextChannel, guild: Guild): Boolean {
        if (!canExecute(user, channel, guild)) {
            val sb = StringBuilder()
            sb.appendln(permissionType.errorMsg)
            sb.appendln("Please execute this command in: ")
            allowedChannels.forEach {
                sb.appendln("#$it")
            }
            channel.createMessage { it.setContent(user.mention + ", $sb") }
            return true
        }

        if (command.size != 3) {
            return false
        }

        val ckey = command[2]

        try {
            val connection = Socket(Constants.SERVER_IP, Constants.SERVER_PORT)
            connection.use { socket ->
                socket.soTimeout = 2000
                val writer = socket.getOutputStream()
                val reader = DataInputStream(socket.getInputStream())

                //Sanitize CKey before sending to server
                val safeCkey = URLEncoder.encode(ckey, Charsets.ISO_8859_1.name())

                val query = ByondPacket("?notes=$safeCkey&key=${Constants.COMMS_KEY}")
                query.sendPacket(writer)

                //This defaults to a response size up to ~10000 chars. Should be enough for notes?
                val results = ByondResponsePacket.fromInputStream(reader)

                //Notes don't split nicely, so toMap() doesn't work correctly
                val notes = results.toString()

                //We've been locked out. We need to wait 1 minute or we will be throttled.
                if (notes.startsWith("Bad Key")) {
                    channel.createMessage { it.setContent("${user.mention}, communication locked. Please wait 1 minute to retry.") }.subscribe()
                    return true
                }

                //No notes found
                if (notes == "No information found on the given key.") {
                    channel.createMessage { it.setContent("${user.mention}, no notes exist for that player. Double check ckey?") }.subscribe()
                    return true
                }

                //Split each note into a separate paragraph before printing
                val message = StringBuilder()
                message.append("\n")
                notes.split("\n").forEach {
                    message.append(it + "\n")
                }

                //Remove a bunch of spaces BYOND sends for some reason
                channel.createMessage { it.setContent("${user.mention}, ${message.toString().replace("          ", "")}") }.subscribe()
            }
        } catch (ex: ConnectException) {
            channel.createMessage { it.setContent("${user.mention}, I am unable to connect to the server!") }.subscribe()
        }

        return true
    }

    override fun getUsage(): String = "$command [ckey]"
}