package org.projectunsc.cicero

import com.beust.jcommander.JCommander
import com.beust.jcommander.Parameter
import discord4j.core.DiscordClient
import discord4j.core.DiscordClientBuilder
import discord4j.core.`object`.entity.TextChannel
import discord4j.core.`object`.util.Snowflake
import discord4j.core.event.domain.guild.GuildCreateEvent
import discord4j.core.event.domain.lifecycle.ReadyEvent
import org.projectunsc.cicero.commands.staff.LogsCommand
import org.projectunsc.cicero.web.WebInterface
import java.util.*

/**
 * Main
 * @author Gregory Maddra
 * 2017-05-23
 */
object Main {

    lateinit var musicTextChannel: TextChannel

    @Parameter(names = ["--musicVoice", "-mv"], description = "Channel ID to use for playing music")
    var musicVoiceChannelID: Long = 0

    @Parameter(names = ["--musicChat", "-mc", "-mt"], description = "Channel ID to use for music feedback and control")
    var musicTextChannelID: Long = 0

    @Parameter(names = ["--musicGuild", "-mg"], description = "Guild to play music in")
    var mainGuildID: Long = 0

    @Parameter(names = ["--token", "-t"], description = "Bot token", password = true, echoInput = true)
    var token: String = ""

    lateinit var client: DiscordClient private set

    private fun createClient(token: String): DiscordClient? {
        return DiscordClientBuilder(token).build()
    }

    @JvmStatic
    fun main(args: Array<String>) {
        //Parse command line arguments
        JCommander.newBuilder()
                .addObject(arrayOf(this, Constants, LogsCommand, WebInterface))
                .build()
                .parse(*args)

        //Create a client, and login
        client = createClient(token) ?: throw Exception("Unable to create client!")
        client.eventDispatcher.on(ReadyEvent::class.java)
                .map { event -> event.guilds.size }
                .flatMap { size ->
                    client.eventDispatcher.on(GuildCreateEvent::class.java)
                            .take(size.toLong())
                            .collectList()
                }
                .subscribe {
                    //Get the channel to use for controlling music player
                    client.getChannelById(Snowflake.of(musicTextChannelID)).subscribe {
                        musicTextChannel = it as TextChannel
                    }

                    CiceroBot(client)
                    WebInterface.start()
                }
        client.login().subscribe()

        //Run a primitive console for controlling the bot
        val consoleInput = Scanner(System.`in`)
        commandLoop@ while(consoleInput.hasNext()){
            val command = consoleInput.next()
            if(command.startsWith("/")) {
                when(command.replace("/", "")){
                    "logout" -> {
                        client.logout().block()
                        WebInterface.stop()
                        break@commandLoop
                    }
                }
            }
        }
    }
}