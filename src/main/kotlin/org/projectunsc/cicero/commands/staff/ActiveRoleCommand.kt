package org.projectunsc.cicero.commands.staff

import discord4j.core.`object`.entity.Guild
import discord4j.core.`object`.entity.TextChannel
import discord4j.core.`object`.entity.User
import discord4j.core.`object`.util.Snowflake
import org.projectunsc.cicero.Constants
import org.projectunsc.cicero.Main
import org.projectunsc.cicero.commands.AbstractCommand

object ActiveRoleCommand : AbstractCommand() {
    override val permissionType: CommandPermissionType = CommandPermissionType.ROLE

    private val ACTIVE_ROLE = Snowflake.of(Constants.ACTIVE_STAFF_ROLE_ID)
    private val INACTIVE_ROLE = Snowflake.of(Constants.INACTIVE_STAFF_ROLE_ID)

    init {
        Main.client.getRoleById(Snowflake.of(Main.mainGuildID), ACTIVE_ROLE).subscribe {
            allowedRoles.add(it)
        }
        Main.client.getRoleById(Snowflake.of(Main.mainGuildID), INACTIVE_ROLE).subscribe {
            allowedRoles.add(it)
        }
    }

    override val command = "staff-status"

    override fun execute(command: List<String>, user: User, channel: TextChannel, guild: Guild): Boolean {
        if (!canExecute(user, channel, guild)) {
            channel.createMessage {
                it.setContent("${user.mention}, you must currently be (in)active staff to toggle your staff status!")
            }.subscribe()
            return true
        }

        guild.members.filter { it.id == user.id }.subscribe { m ->
            m.roles.filter { it.id == ACTIVE_ROLE || it.id == INACTIVE_ROLE }.take(1).subscribe { role ->
                if (role.id == ACTIVE_ROLE) {
                    m.removeRole(ACTIVE_ROLE).subscribe {
                        throw Exception("Unable to remove role!")
                    }
                    m.addRole(INACTIVE_ROLE).subscribe {
                        throw Exception("Unable to add role!")
                    }
                    channel.createMessage("${user.mention}: Role updated to Inactive!").subscribe()
                } else {
                    m.removeRole(INACTIVE_ROLE).subscribe {
                        throw Exception("Unable to remove role!")
                    }
                    m.addRole(ACTIVE_ROLE).subscribe {
                        throw Exception("Unable to add role!")
                    }
                    channel.createMessage("${user.mention}: Role updated to active!").subscribe()
                }
            }
        }

        return true
    }
}