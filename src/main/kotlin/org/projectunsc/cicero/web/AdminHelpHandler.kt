package org.projectunsc.cicero.web

import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpHandler
import discord4j.core.`object`.entity.TextChannel
import discord4j.core.`object`.util.Snowflake
import discord4j.core.spec.EmbedCreateSpec
import org.projectunsc.cicero.Constants
import org.projectunsc.cicero.Main
import java.awt.Color
import java.net.URLDecoder
import java.time.Instant

object AdminHelpHandler : HttpHandler {

    private val channels = Main.client.getGuildById(Snowflake.of(Main.mainGuildID)).block()?.channels?.collectList()?.block()
    private val oniChannel = (channels?.first { it.name == "office-of-naval-intelligence" }
            ?: throw Exception("Couldn't find ONI channel")) as TextChannel
    private val alertChannel = (channels?.first { it.name == "cicero-logs-notes-alerts" }
            ?: throw Exception("Couldn't find Cicero alerts channel")) as TextChannel

    fun createEmbed(embed: EmbedCreateSpec, parameters: Map<String, String>): Boolean {
        val text = URLDecoder.decode(parameters["message"], Charsets.ISO_8859_1.name())
        var noAdmins = false

        embed.setTitle("New ${parameters["type"]}")
        embed.setAuthor("${parameters["src"]} (${parameters["src_char"]})",
                "https://secure.byond.com/members/${parameters["src"]?.replace(" ", "")}", Main.client.self.block()?.avatarUrl)
        embed.setColor(Color.BLUE)

        embed.setDescription(text.take(2048).replace("@everyone", "(at)everyone", ignoreCase = true))
        when (parameters["type"]) {
            "ircpm" -> {
                val target = oniChannel.guild.block()?.members?.collectList()?.block()?.find { it.displayName == parameters["target"] }?.mention
                        ?: "Unknown User"
                embed.addField("Sent to", target, true)
            }
            "adminpm" -> {
                embed.addField("Sent to", "${parameters["trg_key"]} (${parameters["trg_char"]})", true)
            }
            "adminhelp" -> {
                if (text.contains("!!All admins AFK") || text.contains("!!No admins online")) {
                    noAdmins = true
                }

                embed.addField("Sent to", "Everyone", true)
            }
        }

        if (text.length > 2048) {
            embed.addField("Message Truncated?", "Yes", true)
        } else {
            embed.addField("Message Truncated?", "No", true)
        }

        embed.setTimestamp(Instant.now())

        return noAdmins
    }

    override fun handle(exchange: HttpExchange) {
        if (!exchange.requestMethod.equals("get", true)) {
            exchange.sendResponseHeaders(405, 0)
            exchange.close()
            return
        }

        val parameters = WebInterface.handleGetParameters(exchange.requestURI.query)
        if (parameters.getOrDefault("key", "") != Constants.COMMS_KEY) {
            exchange.sendResponseHeaders(403, 0)
            exchange.close()
            return
        }

        if (parameters["message"] == null) {
            exchange.sendResponseHeaders(400, 0)
            exchange.close()
            return
        }

        var noAdmins = false
        alertChannel.createEmbed { embed ->
            noAdmins = createEmbed(embed, parameters)
        }.subscribe()

        if (noAdmins) {
            oniChannel.createMessage {
                it.setContent("@here, a new ${parameters["type"]} was received, but no admins were available to handle it!")
                it.setEmbed { embed ->
                    createEmbed(embed, parameters)
                }
            }.subscribe()
        }

        exchange.sendResponseHeaders(204, -1)
        exchange.close()
    }
}