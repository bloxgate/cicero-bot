package org.projectunsc.cicero.commands.staff

import discord4j.core.`object`.entity.Guild
import discord4j.core.`object`.entity.TextChannel
import discord4j.core.`object`.entity.User
import org.projectunsc.cicero.commands.AbstractCommand
import org.projectunsc.cicero.commands.public.MusicCommand

/**
 * MusicSkipCommand
 * Forces Cicero to skip the current music track
 * @author Gregory Maddra
 * 2017-12-23
 */
object MusicSkipCommand : AbstractCommand() {

    override val permissionType: CommandPermissionType = CommandPermissionType.CHANNEL
    override val command = "force-skip-track"

    init {
        allowedChannels.add("staffbus")
    }

    override fun execute(command: List<String>, user: User, channel: TextChannel, guild: Guild): Boolean {
        if (!canExecute(user, channel, guild)) {
            channel.createMessage { it.setContent("${user.mention}, this command is restricted to #staffbus") }
            return true
        }

        //Call skip method with admin override
        MusicCommand.skipTrackVote(channel, user.username, true)
        return true
    }
}