package org.projectunsc.cicero.commands.staff

import com.beust.jcommander.Parameter
import discord4j.core.`object`.entity.Guild
import discord4j.core.`object`.entity.TextChannel
import discord4j.core.`object`.entity.User
import org.projectunsc.cicero.commands.AbstractCommand
import java.io.*
import java.nio.file.Paths
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.Year
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

/**
 * Allows retrieving server logs
 * @author Gregory Maddra
 * 2018-04-02
 */
object LogsCommand : AbstractCommand() {

    override var permissionType = CommandPermissionType.CHANNEL
    override var command = "logs"

    @Parameter(names = arrayOf("--logPath"), description = "Location of server logs", required = true)
    private var logPath: String = ""

    //Date parser
    private val dateFormat = SimpleDateFormat("yyyy'/'MM'-'MMMM'/'dd'-'EEEE'.log'")
    private val dateParser = SimpleDateFormat("yyyy MM dd")
    private val year = Year.now().toString()

    //Temporary file location
    private val tempDir: String = System.getProperty("java.io.tmpdir")

    init {
        //This command is restricted to needed channels
        allowedChannels.add("cicero-logs-notes-alerts")
    }

    override fun getUsage(): String {
        return "logs (yyyy) [MM] [dd]"
    }

    override fun execute(command: List<String>, user: User, channel: TextChannel, guild: Guild): Boolean {
        //Check execution
        if (!canExecute(user, channel, guild)) {
            val sb = StringBuilder()
            sb.appendln(permissionType.errorMsg)
            sb.appendln("This command is restricted to: ")
            allowedChannels.forEach { sb.append("#$it") }
            channel.createMessage { it.setContent(user.mention + ", $sb") }.subscribe()
            return true
        }

        //Parse the date
        val log = try {
            when {
            command.size == 4 -> //No year
                dateFormat.format(dateParser.parse(year + " " + command[2] + " " + command[3]))
            command.size == 5 -> //year given
                dateFormat.format(dateParser.parse(command[2] + " " + command[3] + " " + command[4]))
            else -> //Not enough information for date
                return false
            }
        } catch (ex: ParseException) {
            channel.createMessage { it.setContent("${user.mention}, invalid date.") }.subscribe()
            return false
        }

        var logFile = Paths.get(logPath, log).toFile()
        var compressed = false
        if (logFile.length() > 8e+06) //File is over 8MB
        {
            val zip = File("$tempDir/${log.split("/").last()}.zip")
            if (zip.createNewFile()) //Create a zip file if it doesn't already exist
            {
                ZipOutputStream(BufferedOutputStream(FileOutputStream(zip))).use { writer ->
                    BufferedInputStream(FileInputStream(logFile)).use { reader ->
                        writer.putNextEntry(ZipEntry(log)) //Create a new entry in the zip
                        reader.copyTo(writer) //Compress the log file into a zip
                        writer.closeEntry() //Close the entry
                    }
                }
            }
            logFile = zip
            compressed = true
        }

        val stream = logFile.inputStream()
        channel.createMessage { message ->
            try {
                message.setContent("${user.mention}, log retrieved")
                message.addFile(logFile.name, stream)
            } catch (ex: FileNotFoundException) {
                message.setContent("${user.mention}, log not found")
            }
        }.subscribe {
            stream.close()
            if (compressed) {
                while (!logFile.delete()) {

                }
            }
        } //Block so that zip can be safely deleted afterwards if needed

        return true
    }
}