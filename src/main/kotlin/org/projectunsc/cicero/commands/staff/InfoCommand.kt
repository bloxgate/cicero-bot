package org.projectunsc.cicero.commands.staff

import discord4j.core.`object`.entity.Guild
import discord4j.core.`object`.entity.TextChannel
import discord4j.core.`object`.entity.User
import me.gmaddra.byond4k.ByondPacket
import me.gmaddra.byond4k.ByondResponsePacket
import me.gmaddra.byond4k.mobs.MobStates
import org.projectunsc.cicero.Constants
import org.projectunsc.cicero.commands.AbstractCommand
import java.io.DataInputStream
import java.net.ConnectException
import java.net.Socket
import java.net.URLEncoder

/**
 * Retrieves info about a player currently in the game.
 * @author Gregory Maddra
 * 2017-11-14
 */
object InfoCommand : AbstractCommand() {

    override val permissionType: CommandPermissionType = CommandPermissionType.CHANNEL
    override val command = "info"

    init {
        allowedChannels.add("office-of-naval-intelligence")
    }

    override fun execute(command: List<String>, user: User, channel: TextChannel, guild: Guild): Boolean {
        if (!canExecute(user, channel, guild)) {
            val sb = StringBuilder()
            sb.appendln(permissionType.errorMsg)
            sb.appendln("Please run this command in #staffbus!")
            channel.createMessage { message ->
                message.setContent("${user.mention}, $sb")
            }.subscribe()
            return true
        }

        if (command.size != 3) {
            return false
        }

        // Get needed info
        val ckey = command[2]

        try {
            val connection = Socket(Constants.SERVER_IP, Constants.SERVER_PORT)
            connection.use {
                it.soTimeout = 2000
                val writer = it.getOutputStream()
                val reader = DataInputStream(it.getInputStream())

                //Sanitize ckey before sending to server
                val safeCkey = URLEncoder.encode(ckey, Charsets.ISO_8859_1.name())

                val query = ByondPacket("?info=$safeCkey&key=${Constants.COMMS_KEY}")
                query.sendPacket(writer)

                val results = ByondResponsePacket.fromInputStream(reader)

                if (results.toString() == "No matches") {
                    channel.createMessage { it.setContent("${user.mention} No matches were found.") }.subscribe()
                    return true
                }

                //We've been locked out due to a bad comms key. We need to wait 1 minute or get throttled
                if (results.toString().contains("Bad Key")) {
                    channel.createMessage { it.setContent("${user.mention} Communication locked! Please wait 1 minute before retry!") }.subscribe()
                    return true
                }

                val messageParts = results.toMap()
                val info = "\nCKey: ${messageParts["key"]}Name: ${messageParts["name"]}Location: ${messageParts["area"]}" +
                        "Antag Status: ${messageParts["antag"]}Mob Status: ${MobStates.getByStateInt(messageParts["stat"]?.replace("\n", "")?.toInt())}"

                // Send info to staff
                channel.createMessage { it.setContent("${user.mention}, $info") }.subscribe()
            }
        } catch (ex: ConnectException) {
            channel.createMessage { it.setContent("${user.mention}, I am unable to connect to the server!") }.subscribe()
        }

        return true
    }

    override fun getUsage(): String
            = "$command [ckey]"
}