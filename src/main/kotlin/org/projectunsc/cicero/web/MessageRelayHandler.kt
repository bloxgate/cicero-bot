package org.projectunsc.cicero.web

import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpHandler
import discord4j.core.`object`.entity.TextChannel
import discord4j.core.`object`.util.Snowflake
import org.projectunsc.cicero.Constants
import org.projectunsc.cicero.Main
import java.net.URLDecoder

object MessageRelayHandler : HttpHandler {

    override fun handle(exchange: HttpExchange) {
        if (!exchange.requestMethod.equals("get", true)) {
            exchange.sendResponseHeaders(405, 0)
            exchange.close()
            return
        }

        val parameters = WebInterface.handleGetParameters(exchange.requestURI.rawQuery) //May contain encoded symbols - use raw query to avoid early decoding
        if (parameters.getOrDefault("key", "") != Constants.COMMS_KEY) {
            exchange.sendResponseHeaders(403, 0)
            exchange.close()
            return
        }

        if (parameters["channel"] == null || parameters["message"] == null) {
            exchange.sendResponseHeaders(400, 0)
            exchange.close()
            return
        }

        Main.client.getGuildById(Snowflake.of(Main.mainGuildID)).subscribe { guild ->
            guild.channels.filter { it.name == parameters["channel"] }
                    .map { it as TextChannel }
                    .subscribe { channel ->
                        println("Channel processing")
                        val message = URLDecoder.decode(parameters["message"], Charsets.ISO_8859_1.name())
                        if (message.length > 2000) {
                            exchange.sendResponseHeaders(413, 0)
                            exchange.close()
                        } else {
                            //To avoid someone spamming all admins
                            message.replace("@everyone", "(at)everyone", ignoreCase = true)

                            channel.createMessage {
                                it.setContent(message)
                            }.subscribe()

                            exchange.sendResponseHeaders(204, -1)
                            exchange.close()
                        }
                    }
        }
    }
}