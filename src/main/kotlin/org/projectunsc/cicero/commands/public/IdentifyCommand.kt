package org.projectunsc.cicero.commands.public

import discord4j.core.`object`.entity.Guild
import discord4j.core.`object`.entity.TextChannel
import discord4j.core.`object`.entity.User
import org.projectunsc.cicero.commands.AbstractCommand

/**
 * A bit of fluff command to make this bot fit the theme a bit better
 * @author Gregory Maddra
 * 2017-11-13
 */
object IdentifyCommand : AbstractCommand() {
    override val permissionType: CommandPermissionType = CommandPermissionType.DEFAULT
    override val command: String = "identify"

    override fun execute(command: List<String>, user: User, channel: TextChannel, guild: Guild): Boolean {
        if (!canExecute(user, channel, guild)) {
            // this should never happen for this command.
            channel.createMessage { it.setContent("${user.mention}, there was an internal error executing that command") }
                    .subscribe()
            return true
        }

        //Fluff identification message
        channel.createMessage { it.setContent("${user.mention}, I am UNSC AI Cicero. Service Number CCR 3106-7. Version 2.0.1") }
            .subscribe()
        return true
    }
}