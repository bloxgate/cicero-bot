package org.projectunsc.cicero.commands.public

import discord4j.core.`object`.entity.Guild
import discord4j.core.`object`.entity.TextChannel
import discord4j.core.`object`.entity.User
import org.projectunsc.cicero.CiceroBot
import org.projectunsc.cicero.commands.AbstractCommand

/**
 * Prints a list of all valid user commands
 * @author Gregory Maddra
 * @version 2017-10-25
 */
object HelpCommand : AbstractCommand() {

    override val permissionType = CommandPermissionType.DEFAULT
    override val command = "help"

    override fun execute(command: List<String>, user: User, channel: TextChannel, guild: Guild): Boolean {
        if (!canExecute(user, channel, guild)) {
            return false
        }

        channel.createMessage {
            it.setContent(user.mention + ", valid commands include: ")
        }.subscribe()
        var commands = ""
        CiceroBot.commandList.forEach {
            commands += it.command + "\n"
        }
        channel.createMessage {
            it.setContent(commands)
        }.subscribe()
        return true
    }
}