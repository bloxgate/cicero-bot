package org.projectunsc.cicero.commands.public

import discord4j.core.`object`.entity.Guild
import discord4j.core.`object`.entity.TextChannel
import discord4j.core.`object`.entity.User
import org.projectunsc.cicero.commands.AbstractCommand

object LinksCommand : AbstractCommand() {

    override val permissionType: CommandPermissionType = CommandPermissionType.DEFAULT
    override val command = "links"

    private const val message = "Forums: <https://www.projectunsc.org/forum/>\nWiki: <https://www.projectunsc.org/wiki/>\n" +
            "Source Code: <https://github.com/HaloSpaceStation/HaloSpaceStation13>\nJoin Server: <byond://ss13.projectunsc.org:2701>"

    override fun execute(command: List<String>, user: User, channel: TextChannel, guild: Guild): Boolean {
        if (!canExecute(user, channel, guild)) {
            return false
        }

        channel.createMessage { it.setContent("${user.mention}\n$message") }.subscribe()

        return true
    }
}