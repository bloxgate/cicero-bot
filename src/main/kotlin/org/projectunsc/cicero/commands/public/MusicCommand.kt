package org.projectunsc.cicero.commands.public

import com.sedmelluq.discord.lavaplayer.format.StandardAudioDataFormats
import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventAdapter
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers
import com.sedmelluq.discord.lavaplayer.source.youtube.YoutubeAudioSourceManager
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist
import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason
import com.sedmelluq.discord.lavaplayer.track.playback.MutableAudioFrame
import com.sedmelluq.discord.lavaplayer.track.playback.NonAllocatingAudioFrameBuffer
import discord4j.core.`object`.entity.Guild
import discord4j.core.`object`.entity.TextChannel
import discord4j.core.`object`.entity.User
import discord4j.core.`object`.entity.VoiceChannel
import discord4j.core.`object`.util.Snowflake
import discord4j.voice.AudioProvider
import discord4j.voice.VoiceConnection
import org.projectunsc.cicero.Main
import org.projectunsc.cicero.commands.AbstractCommand
import java.nio.ByteBuffer
import java.util.*

/**
 * MusicCommand
 * @author Gregory Maddra
 * 2017-10-26
 */
object MusicCommand : AbstractCommand() {

    override val permissionType = CommandPermissionType.CHANNEL
    override val command = "music"

    private var skipVotes = hashSetOf<String>()
    private var joinedMusic = false
    private val musicChannel = Main.client.getChannelById(Snowflake.of(Main.musicVoiceChannelID)).block() as VoiceChannel
    private val musicQueue = LinkedList<AudioTrack>()
    private val musicGuild = Main.client.getGuildById(Snowflake.of(Main.mainGuildID)).block()
            ?: throw Exception("Music guild not found!")
    private val audioEventListener: TrackEventListener
    private val audioManager: AudioPlayerManager
    private val musicChannelPlayer: AudioPlayer
    private val audioProvider: CiceroAudioProvider

    private var currentChannel: VoiceConnection? = null

    init {
        allowedChannels.add("music")

        //Gets the manager for using Discord with LavaPlayer
        audioManager = DefaultAudioPlayerManager()

        //An optimization from Discord4J
        audioManager.configuration.setFrameBufferFactory { bufferDuration, format, stopping ->
            NonAllocatingAudioFrameBuffer(bufferDuration, format, stopping)
        }

        AudioSourceManagers.registerRemoteSources(audioManager)

        //Gets a player for the music channel
        musicChannelPlayer = audioManager.createPlayer()

        //Register handler for ending and start events
        audioEventListener = TrackEventListener(Main.musicTextChannel, musicChannelPlayer)
        musicChannelPlayer.addListener(audioEventListener)

        //Allow playing of music from YT
        audioManager.registerSourceManager(YoutubeAudioSourceManager(false))
        //Allow playing of music from SoundCloud
        //TODO: FIX
        //audioManager.registerSourceManager(SoundCloudAudioSourceManager(false))

        audioProvider = CiceroAudioProvider(musicChannelPlayer)
    }

    override fun execute(command: List<String>, user: User, channel: TextChannel, guild: Guild): Boolean {
        if (!canExecute(user, channel, guild)) {
            channel.createMessage { it.setContent(user.mention + ", this command can only be used in #music") }.subscribe()
            return true
        }

        if (command.size == 3) {
            return if (command[2] == "skip-track") {
                skipTrackVote(channel, user.username, false)
                true
            } else {
                handleMusic(channel, user, command.getOrNull(2))
                true
            }
        }
        return false
    }

    override fun getUsage(): String {
        val builder = StringBuilder()
        builder.appendln("Usage: ")
        builder.appendln("$command [video ID] - Loads a Youtube video's audio")
        builder.appendln("$command [playlist ID] - Loads audio from a playlist of videos")
        builder.appendln("$command skip-track - Vote to skip the current song")
        return builder.toString()
    }

    /**
     * Allows voting to skip the currently playing track
     * @param channel Which channel was the vote made in
     * @param author Who made the vote?
     * @param adminSkip Is this an admin override of the normal voting process
     */
    fun skipTrackVote(channel: TextChannel, author: String, adminSkip: Boolean) {
        if (channel != Main.musicTextChannel && !adminSkip) {
            channel.createMessage { it.setContent("$author, please make your request in ${Main.musicTextChannel.mention}") }.subscribe()
            return
        }
        skipVotes.add(author)
        var userCount = 0
        musicGuild.members.subscribe { member ->
            member.voiceState.map { vState ->
                vState.channel.filter { channel ->
                    channel == musicChannel
                }.subscribe {
                    userCount++
                }
            }
        }
        //Need a majority of listeners to vote in order to skip
        //Admins can force a skip through staff channel
        if ((skipVotes.size.toDouble() / userCount.toDouble()) >= 0.5 || adminSkip) {
            musicChannelPlayer.stopTrack()
            channel.createMessage { it.setContent("Skipping track!") }.subscribe()
            skipVotes.clear()
        } else {
            val percent = Math.round((skipVotes.size.toDouble() / (userCount.toDouble())) * 100)
            channel.createMessage { it.setContent("$percent% of listeners have voted to skip current track. 50% Needed") }.subscribe()
        }
    }

    /**
     * Handles adding music requests to the queue
     * @param channel Which channel was this message sent in?
     * @param author Who made this music request
     * @param trackID The ID of the youtube track to load
     */
    private fun handleMusic(channel: TextChannel, author: User, trackID: String?) {
        if (trackID == null) {
            //A track is required
            channel.createMessage { it.setContent("${author.mention}\nCommand Usage: music [trackID]") }.subscribe()
            return
        }

        audioManager.loadItem(trackID, AudioLoadHandler(channel))
    }

    /**
     * Toggles the join status of the voice channel
     */
    fun toggleVoice() {
        if (joinedMusic) {
            musicQueue.clear()
            musicChannelPlayer.stopTrack()
            currentChannel?.disconnect()
        } else {
            currentChannel = musicChannel.join {
                it.setProvider(audioProvider)
            }.block()
        }

        //Toggle joinedMusic
        joinedMusic = !joinedMusic
    }

    /**
     * Manages the queue for the music player
     */
    private class AudioLoadHandler(val channel: TextChannel) : AudioLoadResultHandler {

        override fun trackLoaded(track: AudioTrack) {
            channel.createMessage { it.setContent("Queued: ${track.info.title}") }.subscribe {
                audioEventListener.queue(track)
            }
        }

        override fun loadFailed(exception: FriendlyException?) {
            val error = (exception as Throwable).message
            channel.createMessage { it.setContent("Failed to load track. $error") }.subscribe()
        }

        override fun noMatches() {
            channel.createMessage { it.setContent("Video not found") }.subscribe()
        }

        override fun playlistLoaded(playlist: AudioPlaylist) {
            for (track in playlist.tracks) {
                channel.createMessage { it.setContent("Queued: ${track.info.title}") }.subscribe {
                    audioEventListener.queue(track)
                }
            }
        }

    }

    /**
     * Handles starting and stopping tracks
     */
    private class TrackEventListener(val channel: TextChannel, val player: AudioPlayer) : AudioEventAdapter() {

        fun queue(track: AudioTrack) {
            if (!this.player.startTrack(track, true)) {
                musicQueue.offer(track)
            }
        }

        override fun onTrackStart(player: AudioPlayer?, track: AudioTrack) {
            super.onTrackStart(player, track)
            skipVotes.clear() //Clear the skip voting in case a previous vote failed
            if (!joinedMusic) {
                currentChannel = musicChannel.join {
                    it.setProvider(audioProvider)
                }.block()
                joinedMusic = true
            }
            channel.createMessage { it.setContent("\uD83C\uDFB5Now Playing: ${track.info.title} [${Math.round((track.info.length / 1000) / 60.0)} Minutes]\uD83C\uDFB5") }.subscribe()
        }

        override fun onTrackEnd(player: AudioPlayer, track: AudioTrack, endReason: AudioTrackEndReason) {
            super.onTrackEnd(player, track, endReason)

            if (musicQueue.isEmpty()) {
                this.player.stopTrack()
                currentChannel?.disconnect()
                joinedMusic = false
            } else if (endReason.mayStartNext) {
                this.player.startTrack(musicQueue.poll(), false)
            }
        }
    }

    private class CiceroAudioProvider(val player: AudioPlayer) : AudioProvider(ByteBuffer.allocate(StandardAudioDataFormats.DISCORD_OPUS.maximumChunkSize())) {

        private var lastFrame = MutableAudioFrame()

        init {
            lastFrame.setBuffer(buffer)
        }

        override fun provide(): Boolean {
            val provided = player.provide(lastFrame)

            if (provided) {
                buffer.flip()
            }

            return provided
        }

    }
}