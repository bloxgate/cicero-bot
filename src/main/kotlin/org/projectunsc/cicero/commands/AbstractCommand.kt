package org.projectunsc.cicero.commands

import discord4j.core.`object`.entity.Guild
import discord4j.core.`object`.entity.Role
import discord4j.core.`object`.entity.TextChannel
import discord4j.core.`object`.entity.User
import discord4j.core.`object`.util.Permission

/**
 * AbstractCommand
 * Represents a command that may be executed by the Cicero bot
 * @author Gregory Maddra
 * @version 2017-10-25
 */
abstract class AbstractCommand : Comparable<AbstractCommand> {

    /**
     * The list of channel names that this command can be run in
     */
    protected val allowedChannels = mutableListOf<String>()

    /**
     * Permissions required to run this command
     */
    protected val allowedPermissions = mutableListOf<Permission>()

    /**
     * Roles allowed to run this command
     */
    protected val allowedRoles = mutableListOf<Role>()

    /**
     * What type of permission checking should be used for this command
     */
    abstract val permissionType: CommandPermissionType

    /**
     * The trigger for this command
     */
    abstract val command: String

    /**
     * Runs the command
     * @param command The string with parameters
     * @param user the User who ran the command
     * @param channel The channel where the command is run
     * @return true if the command succeeded, false otherwise
     */
    abstract fun execute(command: List<String>, user: User, channel: TextChannel, guild: Guild): Boolean

    /**
     * Is this command run by the given string
     * @param command A command string to check
     * @return true if this command is run by the given string
     */
    fun isCommand(command: String): Boolean {
        return this.command == command
    }

    /**
     * Can this command be run by the caller?
     * @param user The user calling the command
     * @param channel The channel the command is being run in
     * @return true if the command can be run, false otherwise
     */
    fun canExecute(user: User, channel: TextChannel, guild: Guild): Boolean {
        when (permissionType) {
            CommandPermissionType.DEFAULT -> return true
            CommandPermissionType.CHANNEL -> return channel.name in allowedChannels
            CommandPermissionType.PERMISSION -> {
                val roles = user.asMember(guild.id).block()?.roles?.collectList()?.block() ?: emptyList()
                roles.forEach { role ->
                    role.permissions.forEach { perm ->
                        if (perm in allowedPermissions) {
                            return true
                        }
                    }
                }
            }
            CommandPermissionType.ROLE -> {
                val roles = user.asMember(guild.id).block()?.roles?.collectList()?.block() ?: emptyList()
                roles.forEach { role ->
                    if (role in allowedRoles) {
                        return true
                    }
                }
            }
        }
        return false
    }

    /**
     * Provides usage information for this command
     */
    open fun getUsage(): String {
        return "Usage: $command"
    }

    override fun compareTo(other: AbstractCommand): Int {
        return this.command.compareTo(other.command)
    }

    enum class CommandPermissionType(val errorMsg: String) {
        /**
         * The command can only be run in a specific channel.
         */
        CHANNEL("Incorrect channel"),
        /**
         * The user must have a specific role to run the command
         */
        ROLE("Incorrect role"),
        /**
         * The user's role must have a specific permission to run the command
         */
        PERMISSION("Missing Permission"),
        /**
         * The command can always be run
         */
        DEFAULT("Unknown error");



    }
}