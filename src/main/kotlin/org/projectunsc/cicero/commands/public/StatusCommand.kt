package org.projectunsc.cicero.commands.public

import discord4j.core.`object`.entity.Guild
import discord4j.core.`object`.entity.TextChannel
import discord4j.core.`object`.entity.User
import discord4j.core.spec.EmbedCreateSpec
import me.gmaddra.byond4k.ByondPacket
import me.gmaddra.byond4k.ByondResponsePacket
import org.projectunsc.cicero.Constants
import org.projectunsc.cicero.Main
import org.projectunsc.cicero.commands.AbstractCommand
import java.awt.Color
import java.io.DataInputStream
import java.net.ConnectException
import java.net.Socket
import java.net.SocketTimeoutException
import java.time.Instant

/**
 * StatusCommand
 * @author Gregory Maddra
 * 2017-10-25
 */
object StatusCommand : AbstractCommand() {

    override val command = "status"

    override val permissionType = CommandPermissionType.DEFAULT

    override fun execute(command: List<String>, user: User, channel: TextChannel, guild: Guild): Boolean {
        if (!canExecute(user, channel, guild)) {
            return false
        }

        val statusQuery = ByondPacket(Constants.QUERY_STATUS)

        try {
            val commSocket = Socket(Constants.SERVER_IP, Constants.SERVER_PORT)
            commSocket.use { connection ->
                connection.soTimeout = 2000
                val reader = DataInputStream(connection.getInputStream())
                val writer = connection.getOutputStream()

                statusQuery.sendPacket(writer)

                val response = ByondResponsePacket.fromInputStream(reader)
                val resultPairs = response.toMap()

                channel.createEmbed { embed ->

                    buildEmbedBoilderPlate(embed)

                    //Game Info
                    embed.addField("Server Name", resultPairs["version"] ?: "error", true)
                    embed.addField("Game Mode", resultPairs["mode"] ?: "error", true)
                    embed.addField("Current Map", resultPairs["map"] ?: "Not Loaded", true)
                    embed.addField("Players Online", resultPairs["players"] ?: "error", true)
                    embed.addField("Admins Online", resultPairs["admins"] ?: "error", true)
                    embed.addField("Round Duration", resultPairs[Constants.ROUND_DURATION_NAME] ?: "error", true)

                    //Formatting
                    embed.setColor(Color.GREEN)

                    //Footer
                    embed.setFooter("byond://${Constants.SERVER_IP}:${Constants.SERVER_PORT}", null)

                }.subscribe()
            }
        } catch (ex: SocketTimeoutException) {
            channel.createEmbed { embed ->
                buildEmbedBoilderPlate(embed)

                embed.setColor(Color.ORANGE)
                embed.setTitle("Status Unknown")
                embed.setDescription("Cannot determine server status. This may indicate restart.")
            }.subscribe()
        } catch (ex: IllegalArgumentException) {
            //This handles the case where the server sends null information for required fields
            channel.createEmbed { embed ->
                buildEmbedBoilderPlate(embed)

                embed.setColor(Color.ORANGE)
                embed.setTitle("Invalid Data")
                embed.setDescription("The server has sent invalid status information")
            }.subscribe()
        } catch (ex: ConnectException) {
            connectionError(channel)
        }

        return true
    }

    /**
     * Sends a connection failed embed to the given channel
     */
    private fun connectionError(channel: TextChannel) {
        channel.createEmbed { embed ->
            buildEmbedBoilderPlate(embed)

            embed.setColor(Color.RED)
            embed.setTitle("Error!")
            embed.setDescription("Unable to connect to server!")
            embed.setThumbnail("https://projectunsc.org/images/computer-fire.jpg")

        }.subscribe()
    }

    /**
     * Builds a base embed with the correct avatar, URLs, and time
     */
    private fun buildEmbedBoilderPlate(embed: EmbedCreateSpec) {
        embed.setAuthor("HaloStation Server Status: ", "https://www.projectunsc.org", Main.client.self.block()?.avatarUrl)
        embed.setTimestamp(Instant.now())
    }
}