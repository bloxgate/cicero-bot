package org.projectunsc.cicero

import discord4j.core.DiscordClient
import discord4j.core.`object`.entity.Guild
import discord4j.core.`object`.entity.Member
import discord4j.core.`object`.entity.TextChannel
import discord4j.core.`object`.entity.User
import discord4j.core.event.domain.message.MessageCreateEvent
import org.projectunsc.cicero.commands.public.*
import org.projectunsc.cicero.commands.staff.*
import reactor.core.scheduler.Scheduler
import reactor.core.scheduler.Schedulers

/**
 * CiceroBot
 * @author Gregory Maddra
 * 2017-05-23
 */
class CiceroBot(client: DiscordClient) {

    companion object {
        val commandList = sortedSetOf(StatusCommand, HelpCommand, RollTroopsCommand, MusicCommand, IdentifyCommand,
                InfoCommand, NotesCommand, ToggleVoiceCommand, MusicSkipCommand, LogsCommand, LinksCommand, ActiveRoleCommand,
                SubscribeCommand, UnsubscribeCommand)

        val threadPool: Scheduler = Schedulers.newElastic("cicero-elastic")
    }

    var user: User

    init {
        //Registers this Bot to listen to events
        client.eventDispatcher.on(MessageCreateEvent::class.java).publishOn(threadPool).map(this::handle).subscribe()

        user = client.self.block() ?: throw Exception("Unable to get our user!")
    }

    private fun handle(event: MessageCreateEvent) {
        event.message.content.map { content ->
            event.message.channel.subscribe { channel ->
                event.message.guild.subscribe { guild ->
                    event.message.userMentions.collectList().subscribe { mentions ->
                        event.message.authorAsMember.subscribeOn(threadPool).subscribe { author ->
                            parseCommand(content, channel as TextChannel, guild, mentions, event.message.mentionsEveryone(), author)
                        }
                    }
                }
            }
        }
    }

    private fun parseCommand(content: String, channel: TextChannel, guild: Guild, mentions: List<User>, everyone: Boolean, author: Member) {
        //Only process the messgage if it is directed at us
        if (content.startsWith(Constants.PREFIX, ignoreCase = true) || (mentions.contains(user) && !everyone)) {
            //Split the message by spaces
            val messageParts = content.split(" ")

            //We have to have a command to use
            if (messageParts.size < 2) {
                replyToInvalid(channel, author.mention)
            } else {
                try {
                    val command = commandList.first { it.isCommand(messageParts[1].toLowerCase()) }
                    if (!command.execute(messageParts, author, channel, guild)) {
                        channel.createMessage { message ->
                            message.setContent(command.getUsage())
                        }.subscribe()
                    }
                } catch (ex: NoSuchElementException) {
                    replyToInvalid(channel, author.mention)
                }
            }
        }
    }

    /**
     * Replies to an invalid command, informing the user of their mistake
     * @param channel The channel to reply in
     * @param author Who wrote the invalid command
     */
    private fun replyToInvalid(channel: TextChannel, author: String) {
        channel.createMessage { message ->
            message.setContent("$author, that does not appear to be a valid command.")
        }.subscribe()
    }


}