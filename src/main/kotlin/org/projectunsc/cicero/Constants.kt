package org.projectunsc.cicero

import com.beust.jcommander.Parameter

/**
 * Constants
 * @author Gregory Maddra
 * 2017-05-23
 */
object Constants {

    const val PREFIX = "Cicero,"
    const val SERVER_IP = "ss13.projectunsc.org"
    const val SERVER_PORT = 2701

    const val QUERY_STATUS = "?status"

    const val ROUND_DURATION_NAME = "roundduration"

    @Parameter(names = ["--commsKey", "-ck"], description = "Key to use for communicating with the game", password = true)
    var COMMS_KEY = ""

    @Parameter(names = ["--activeStaffID"], description = "Role snowflake for active staff role")
    var ACTIVE_STAFF_ROLE_ID = 675856779598364691L

    @Parameter(names = ["--inactiveStaffID"], description = "Role snowflake for inactive staff role")
    var INACTIVE_STAFF_ROLE_ID = 675856890533511186L

    @Parameter(names = ["--roundAlertRole"], description = "Role snowflake for new round subscribers")
    var ROUND_SUBSCRIBER_ROLE_ID = 565590997250080778L
}