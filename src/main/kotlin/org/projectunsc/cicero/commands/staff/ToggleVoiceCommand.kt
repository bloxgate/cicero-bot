package org.projectunsc.cicero.commands.staff

import discord4j.core.`object`.entity.Guild
import discord4j.core.`object`.entity.TextChannel
import discord4j.core.`object`.entity.User
import org.projectunsc.cicero.commands.AbstractCommand

/**
 * ToggleVoiceCommand
 * Toggle's Cicero's music channel join status
 * @author Gregory Maddra
 * 2017-12-23
 */
object ToggleVoiceCommand : AbstractCommand() {

    override val permissionType: CommandPermissionType = CommandPermissionType.CHANNEL
    override val command = "toggle-voice"

    init {
        allowedChannels.add("staffbus")
    }

    override fun execute(command: List<String>, user: User, channel: TextChannel, guild: Guild): Boolean {
        if (!canExecute(user, channel, guild)) {
            channel.createMessage { it.setContent("${user.mention}, this command is restricted to #staffbus") }.subscribe()
            return true
        }

        //TODO: Re-enable: MusicCommand.toggleVoice()
        return true
    }
}